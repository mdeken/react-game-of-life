# React Game of Life

A simple game of life developed with React and Redux. For those who aren't familiar with the rules I encourage you to read them on the game of life [wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) page.

A deployed version of this project can be found [here](https://react-gameoflife.firebaseapp.com/)

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.14.2

    $ npm --version
    6.11.2

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://gitlab.com/mdeken/react-game-of-life.git
    $ cd react-game-of-life
    $ npm install

## Start & watch

    $ npm start

## Simple build for production

    $ npm run build

---

## Languages & tools

### JavaScript

- [React](http://facebook.github.io/react) is used for UI.
- [axios](https://github.com/axios/axios) is used to handle http requests and errors with interceptors.
- [redux](https://redux.js.org/) is used to seperate rendering logic from gaming logic.
- [redux-thunk](https://github.com/reduxjs/redux-thunk) is used to be able to write asynchronous code in a redux reducer.

### CSS

- CSS is home-made and is inspired by material design [guidelines](https://material.io/components)

