import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

import classes from './Game.module.css';
import { ReactComponent as MenuIcon } from '../../assets/icons/menu.svg';

import Grid from '../../components/Grid/Grid';
import Button from '../../components/UI/Button/Button';
import Presets from '../../components/Presets/Presets';
import SideDrawer from '../../components/UI/SideDrawer/SideDrawer';
import Spinner from '../../components/UI/Spinner/Spinner';

class Game extends Component {
  state = {
    intervalId: null,
    showSideDrawer: false,
  }

  componentDidMount() {
    this.props.onFetchPresets();
  }

  componentDidUpdate() {
    if (this.props.isRunning && this.state.intervalId === null) {
      const intervalId = setInterval(() => this.props.onNextGeneration(), 100);
      this.setState({ intervalId: intervalId });
    } else if (!this.props.isRunning && this.state.intervalId) {
      clearInterval(this.state.intervalId);
      this.setState({ intervalId: null })
    }
  }

  componentWillUnmount() {
    if (this.state.intervalId) {
      clearInterval(this.state.intervalId);
    }
  };

  closeSideDrawer = () => {
    this.setState({showSideDrawer: false});
  };

  openSideDrawer = () => {
    this.setState({showSideDrawer: true});
  };

  presetLoadHandler = (presetGrid) => {
    this.props.onLoadPreset(presetGrid);
    this.closeSideDrawer();
  }

  render() {
    return (
      <div className={classes.Game}>
        <div className={classes.Header}>
          <MenuIcon onClick={this.openSideDrawer}/>
          <h1>Conway's Game of Life</h1>
        </div>
        <SideDrawer
          show={this.state.showSideDrawer}
          closed={this.closeSideDrawer}
        >
        { this.props.isPresetsLoading ? <Spinner className={classes.Spinner}/> :
          <Presets
            presets={this.props.presets}
            presetChanged={this.presetLoadHandler}
            presetExpanded={this.props.onExpandPreset}
          />
        }
        </SideDrawer>
        <div className={classes.Grid}>
          <Grid
            grid={this.props.grid}
            cellClicked={this.props.onToggleCell}
          />
          <div className={classes.ButtonPanel}>
            <Button
              clicked={ this.props.isRunning? this.props.onPauseGame : this.props.onStartGame }
              disabled={this.props.isEmpty}
            >{ this.props.isRunning ? 'Pause' : 'Start' }</Button>
            <Button
              clicked={this.props.onClearGrid}
              type='danger'
              disabled={this.props.isEmpty}
            >Clear</Button>
            <Button
              clicked={this.props.onLoadRandom}
            >Random Cells</Button>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    presets: state.preset.presets,
    isPresetsLoading: state.preset.isLoading,
    grid: state.game.grid,
    isRunning: state.game.isRunning,
    isEmpty: state.game.isEmpty
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPresets: () => dispatch(actions.fetchPresets()),
    onExpandPreset: (presetId) => dispatch(actions.expandPreset(presetId)),
    onToggleCell: (x, y) => dispatch(actions.toggleCell(x, y)),
    onLoadPreset: (presetGrid) => dispatch(actions.loadPreset(presetGrid)),
    onLoadRandom: () => dispatch(actions.loadRandom()),
    onStartGame: () => dispatch(actions.startGame()),
    onPauseGame: () => dispatch(actions.pauseGame()),
    onNextGeneration: () => dispatch(actions.nextGeneration()),
    onClearGrid: () => dispatch(actions.clearGrid()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Game);

