export const updateObject = (oldObject, updatedObject) => {
  return {
    ...oldObject,
    ...updatedObject
  };
};

export const countNeighbors = (grid, cellX, cellY) => {
  let neighbors = 0;
  for(let neighborX = -1; neighborX <= 1 ; neighborX++) {
    for (let neighborY = -1; neighborY <= 1 ; neighborY++) {
      // Dont count the selected cell...
      if (!(neighborX === 0 && neighborY === 0)) {
        const x = cellX + neighborX;
        const y = cellY + neighborY;
        if (grid[x] && grid[x][y] && grid[x][y].isAlive) {
          neighbors++;
        }
      }
    }
  }
  return neighbors;
};

export const isGridEmpty = (grid) => {
  return grid.every((row) => row.every(cell => !cell.isAlive))
}