import React, { Component } from 'react';

const withTimer = (TimedComponent, duration) => {
  return class extends Component {
    state = { show: false };

    componentDidUpdate(prevProps, prevState) {
      if (prevProps.error !== this.props.error) {
        this.setState({show: true});
        setTimeout(() => this.setState({show: false}), duration);
      }
    }

    render() {
      return (
        <React.Fragment>
          <TimedComponent show={this.state.show } { ...this.props } />
        </React.Fragment>
      );
    }
  }
}

export default withTimer;