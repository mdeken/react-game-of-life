import React, { Component } from 'react';
import Snackbar from '../components/UI/Snackbar/Snackbar';

const withHttpError = (WrappedComponent, axios) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {error: null};
      this.reqInterceptor = axios.interceptors.request.use(req => {
        return req;
      });
      this.resInterceptor = axios.interceptors.response.use(res => res, error => {
        const errorObject = new Error(error);
        this.setState({ error: errorObject });
      })
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInterceptor);
      axios.interceptors.response.eject(this.resInterceptor);
    }

    render() {
      return (
        <React.Fragment>
          <Snackbar
            error={this.state.error}
          >
            {this.state.error ? this.state.error.message : null}
          </Snackbar>
          <WrappedComponent {...this.props}/>
        </React.Fragment>
      );
    }
  };

}

export default withHttpError;