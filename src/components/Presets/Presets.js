import React from 'react';

import classes from './Presets.module.css';

import Card from '../UI/Card/Card';

const Presets = (props) => {
  let presets = null;

  if (props.presets) {
    presets = Object.entries(props.presets).map(([id, preset], i) => (
      <Card
        key={id}
        title={preset.name}
        clicked={() => props.presetExpanded(id)}
        description={preset.description}
        isExpanded={preset.isExpanded}
        mediaUrl={preset.image_url}
        mediaAlt={preset.name}
        ctaAction={() => props.presetChanged(preset.grid)}
        ctaText="Charge preset"
      />
    ));
  }
  return (
    <div className={classes.Presets}>
      <ul>
        {presets}
      </ul>
    </div>
  );
}

export default Presets;