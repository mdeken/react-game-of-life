import React from 'react';

import Cell from '../Cell/Cell';

const Grid = (props) => {
  const gridStyle = {
    display: 'grid',
    gridTemplateColumns: `repeat(${props.grid.length}, 1fr)`,
    gridGap: '0',
    width: '100%'
  };

  const cells = props.grid.map((rows, x) => {
    return rows.map((cell, y) => (
      <Cell
        key={`${x},${y}`}
        isAlive={cell.isAlive}
        clicked={props.cellClicked}
        x={x}
        y={y}
      />
    ));
  });

  return (
    <div style={gridStyle}>
      {cells}
    </div>
  );
};

export default Grid;