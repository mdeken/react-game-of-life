import React from 'react';

import classes from './Cell.module.css';
import { ReactComponent as Cross } from '../../assets/icons/cross.svg';


const Cell = (props) => {
  const classList = [classes.Cell];
  const lifeState = (props.isAlive) ? 'alive' : 'dead';
  classList.push(classes[lifeState]);
  
  return (
    <div
      className={classList.join(' ')}
      onClick={() => props.clicked(props.x, props.y)}
    >
      <Cross />
    </div>
  );
};

export default React.memo(Cell);