import React from 'react';

import classes from './Card.module.css';
import { ReactComponent as ExpandArrow } from '../../../assets/icons/expand_arrow.svg';
import Button from '../../UI/Button/Button';

const Card = (props) => {
  return (
    <li className={classes.Card} onClick={props.clicked}>
      <div>
        <img src={props.mediaUrl} alt={props.mediaAlt}/>
      </div>
      <div className={props.isExpanded ? classes.Expanded : null}>
        <div className={classes.Header}>
          <h3>{props.title}</h3>
          <ExpandArrow className={classes.Arrow} />
        </div>
        <div className={classes.Content}>
          <p>{props.description}</p>
          <div>
            <Button clicked={props.ctaAction}>{props.ctaText}</Button>
          </div>
        </div>
      </div>
    </li>
  );
}

export default Card;