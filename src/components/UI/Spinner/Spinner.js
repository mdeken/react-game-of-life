import React from 'react';

import classes from './Spinner.module.css';

const Spinner = (props) => {
  const spinnerClasses = [classes.Spinner];
  if (props.className) {
    spinnerClasses.push(props.className);
  }
  return (
    <div className={spinnerClasses.join(' ')}></div>
  );
};

export default Spinner;