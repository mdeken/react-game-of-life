import React from 'react';

import classes from './Snackbar.module.css';

import withTimer from '../../../hoc/withTimer';

const Snackbar = (props) => {
  const snackbarClasses = [classes.Snackbar];

  if (props.show) {
    snackbarClasses.push(classes.show);
  }
  return (
    <div className={snackbarClasses.join(' ')}>
        <p>{props.children}</p>
    </div>
  );
};

export default withTimer(Snackbar, 5000);