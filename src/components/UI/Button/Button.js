import React from 'react';

import classes from './Button.module.css';

const Button = (props) => {
  const buttonClasses = [classes.Button];
  if (props.className) {
    buttonClasses.push(props.className);
  }
  switch(props.type) {
    case 'danger':
      buttonClasses.push(classes.danger);
    break;
    default: 
  }
  return (
    <button disabled={props.disabled} className={buttonClasses.join(' ')} onClick={props.clicked}>{props.children}</button>
  );
};

export default Button;