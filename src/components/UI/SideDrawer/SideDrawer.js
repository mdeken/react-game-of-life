import React from 'react';
import axios from 'axios';

import classes from './SideDrawer.module.css';
import { ReactComponent as CloseIcon } from '../../../assets/icons/cross.svg';

import Overlay from '../Overlay/Overlay';
import withHttpError from '../../../hoc/withHttpError';

const SideDrawer = (props) => {
  const drawerClasses = [classes.SideDrawer];
  if (!props.show) {
    drawerClasses.push(classes.closed);
  }
  return (
    <React.Fragment>
      {props.show ? <Overlay clicked={props.closed}/> : null}
      <div className={drawerClasses.join(' ')}>
        <div className={classes.Header}>
          <h2>Presets</h2>
          <CloseIcon className={classes.CloseIcon} onClick={props.closed}/>
        </div>
        {props.children}
      </div>
    </React.Fragment>
  );
};

export default withHttpError(SideDrawer, axios);