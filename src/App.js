import React from 'react';

import Game from './page/Game/Game';
import classes from './App.module.css';

function App() {
  return (
    <div className={classes.App}>
      <Game />
    </div>
  );
}

export default App;
