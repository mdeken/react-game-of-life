import * as actionTypes from './actionTypes';

export const toggleCell = (x, y) => {
  return {
    type: actionTypes.TOGGLE_CELL,
    x: x,
    y: y
  };
};

export const loadPreset = (presetGrid) => {
  return {
    type: actionTypes.LOAD_PRESET,
    presetGrid: presetGrid
  };
};

export const loadRandom = () => {
  return {
    type: actionTypes.LOAD_RANDOM
  };
};

export const startGame = () => {
  return {
    type: actionTypes.START_GAME,
  };
};

export const pauseGame = () => {
  return {
    type: actionTypes.PAUSE_GAME,
  };
};

export const nextGeneration = () => {
  return {
    type: actionTypes.NEXT_GENERATION,
  };
};

export const clearGrid = () => {
  return {
    type: actionTypes.CLEAR_GRID,
  };
};