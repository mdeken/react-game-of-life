import * as actionTypes from './actionTypes';
import axios from 'axios';

export const fetchPresetsStart = () => {
  return {
    type: actionTypes.FETCH_PRESETS_START,
  };
};

export const fetchPresetsSucceed = (presets) => {
  return {
    type: actionTypes.FETCH_PRESETS_SUCCEED,
    presets: presets
  };
};

export const fetchPresetsFail = (error) => {
  return {
    type: actionTypes.FETCH_PRESETS_FAIL,
    error: error
  };
};

export const fetchPresets = () => {
  return dispatch => {
    dispatch(fetchPresetsStart());
    axios.get('https://react-gameoflife.firebaseio.com/presets.json')
      .then(result => {
        const presets = Object.entries(result.data).map(([id, preset]) => ({ id: id, ...preset }))
        return presets;
      })
      .then(presets => dispatch(fetchPresetsSucceed(presets)))
      .catch(error => {
        dispatch(fetchPresetsFail(error))
      });
  };
};

export const expandPreset = (presetId) => {
  return {
    type: actionTypes.EXPAND_PRESET,
    presetId: presetId,
  };
};