export {
  fetchPresets,
  expandPreset
} from './preset';
export {
  toggleCell,
  loadPreset,
  loadRandom,
  startGame,
  pauseGame,
  nextGeneration,
  clearGrid
} from './game';