import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const fetchPresetsStart = (state, action) => {
  return updateObject(state, {isLoading: true});
};

const fetchPresetsSucceed = (state, action) => {
  return updateObject(state, {
    presets: action.presets,
    isLoading: false
  });
};

const fetchPresetsFail = (state, action) => {
  return updateObject(state, {isLoading: true});
};

const expandPreset = (state, action) => {
  const updatedPreset = updateObject(state.presets[action.presetId], {isExpanded: !state.presets[action.presetId].isExpanded});
  const updatedPresets = updateObject(state.presets, {[action.presetId]: updatedPreset});
  return updateObject(state, { presets: updatedPresets });
}

const initialState = {
  presets: {},
  isLoading: false
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.FETCH_PRESETS_START: return fetchPresetsStart(state, action);
    case actionTypes.FETCH_PRESETS_SUCCEED: return fetchPresetsSucceed(state, action);
    case actionTypes.FETCH_PRESETS_FAIL: return fetchPresetsFail(state, action);
    case actionTypes.EXPAND_PRESET: return expandPreset(state, action);
    default: return state;
  }
};

export default reducer;