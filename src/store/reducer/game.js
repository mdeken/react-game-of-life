import * as actionTypes from '../actions/actionTypes';
import { updateObject, countNeighbors, isGridEmpty } from '../../shared/utility';

const GRID_X = 25;
const GRID_Y = 25;
const CELL_STATE = {
  isAlive: false,
};
const GRID_INITIAL = Array(GRID_X).fill(Array(GRID_Y).fill(CELL_STATE));

const initialState = {
  grid: GRID_INITIAL,
  isEmpty: true,
  isRunning: false,
};

const toggleCell = (state, action) => {
  const { x, y } = action;
  const newGrid = state.grid.map(row => row.map(cell => ({ ...cell })));
  newGrid[x][y].isAlive = !newGrid[x][y].isAlive;
  return updateObject(state, {grid: newGrid, isEmpty: isGridEmpty(newGrid)});
};

const loadPreset = (state, action) => {
  const newGrid = action.presetGrid.map(row => row.map(cell => ({ ...cell })));
  return updateObject(state, {grid: newGrid, isEmpty: false});
};

const loadRandom = (state, action) => {
  const randomGrid = state.grid.map((row) => row.map((cell) => ({
    ...cell,
    isAlive: Math.random() > 0.5
  })));
  return updateObject(state, { grid: randomGrid, isEmpty: false});
};

const startGame = (state, action) => {
  return updateObject(state, { isRunning: true });
};

const pauseGame = (state, action) => {
  return updateObject(state, { isRunning: false });
};

const nextGeneration = (state, action) => {
  const nextGrid = state.grid.map((row, x) => (
    row.map((cell, y) => {
      const aliveNeighbors = countNeighbors(state.grid, x, y);
      if (!cell.isAlive && aliveNeighbors === 3) {
        return { ...cell, isAlive: true };
      } else if (cell.isAlive && !(aliveNeighbors === 2 || aliveNeighbors === 3)) {
        return { ...cell, isAlive: false };
      }
      return { ...cell }
    })
  ));
  if (isGridEmpty(nextGrid)) {
    return updateObject(state, { ...initialState });
  } else {
    return updateObject(state, { grid: nextGrid });
  }
};

const clearGrid = (state, action) => {
  return updateObject(state, { ...initialState });
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.TOGGLE_CELL: return toggleCell(state, action);
    case actionTypes.LOAD_PRESET: return loadPreset(state, action);
    case actionTypes.LOAD_RANDOM: return loadRandom(state, action);
    case actionTypes.START_GAME: return startGame(state, action);
    case actionTypes.PAUSE_GAME: return pauseGame(state, action);
    case actionTypes.NEXT_GENERATION: return nextGeneration(state, action);
    case actionTypes.CLEAR_GRID: return clearGrid(state, action);
    default: return state;
  }
};

export default reducer;